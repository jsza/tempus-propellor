{-# LANGUAGE TemplateHaskell #-}

import Propellor
import qualified Propellor.Property.Apt as Apt
import           Propellor.Property.Bootstrap
import qualified Propellor.Property.Cron as Cron
import qualified Propellor.Property.File as File
import qualified Propellor.Property.Git as Git
import qualified Propellor.Property.Hostname as Hostname
import qualified Propellor.Property.Locale as Locale
import qualified Propellor.Property.User as User
import qualified Propellor.Property.Ssh as Ssh
import qualified Propellor.Property.Sudo as Sudo
import qualified Propellor.Property.Systemd as Systemd
import qualified Propellor.Property.SiteSpecific.JayessSites as JayessSites
import qualified Propellor.Property.SiteSpecific.Caddy as Caddy
import           Utility.Embed


main :: IO ()
main = defaultMain hosts


hosts :: [Host]
hosts =
	[ tempus
	]

tempus :: Host
tempus = host "tempus.xyz" $ props
	& osDebian (Stable "buster") X86_64
	& bootstrapWith (Robustly Stack)
	& ipv4 "144.76.229.164"
	& Hostname.sane
	& Hostname.searchDomain
	& Locale.available "en_US.UTF-8"
	& Apt.stdSourcesList `onChange` Apt.update
	& Apt.unattendedUpgrades
	& Apt.installed ["etckeeper"]
	& Git.repoConfigured "/etc/" ("user.name", "root")
  & Git.repoConfigured "/etc/" ("user.email", "root@tempus.xyz")
	& Apt.installed ["ssh", "sudo", "git", "duplicity", "curl", "vim",
	                 "screen", "less", "fish", "htop"]

	-- Users
  & Ssh.noPasswords
  & User.hasSomePassword (User "root")
  & propertyList "admin accounts"
    (toProps $
      [ User.accountFor
      , User.lockedPassword
      ] <*> admins)
  & Sudo.enabledFor (User "jayess")
  & Sudo.enabledFor (User "scotch")
  & adminKeys (User "root")
  & jayessKeys (User "jayess")
  & scotchKeys (User "scotch")

  -- Docker
  & Apt.installed ["docker-ce"]
    `requires` Apt.setSourcesListD ["deb [arch=amd64] https://download.docker.com/linux/debian buster stable"] "docker"
    `requires` Apt.installed ["apt-transport-https"]
    `requires` Apt.trustsKey dockerKey
  & propertyList "admin docker access"
    (toProps (flip User.hasGroup (Group "docker") <$> admins))

  -- Tempus Web
  & File.hasContent "/etc/systemd/system/tempus-backend.service" $(sourceFile "files/tempus-backend/tempus-backend.service")
                    `onChange` Systemd.daemonReloaded
  & Systemd.enabled ("tempus-backend")
  & File.hasContent "/srv/tempus-backend/docker-compose.yaml" $(sourceFile "files/tempus-backend/docker-compose.yaml")
    -- `requires` File.hasPrivContent "/srv/postgrest/postgrest.env" hostContext
    `requires` File.hasPrivContent "/srv/restic-pg-dump/restic-pg-dump.env" hostContext
    `requires` File.dirExists "/srv/restic-pg-dump"
    `requires` File.dirExists "/srv/middleman"
    `requires` File.dirExists "/srv/veil"
    `requires` File.hasPrivContent "/srv/middleman/config.json" hostContext
    `requires` File.hasContent "/srv/middleman/dynamic_roles.json" $(sourceFile "files/middleman/dynamic_roles.json")
    `requires` File.hasPrivContent "/srv/veil/config.json" hostContext
    `requires` Apt.installed ["docker-compose"]
    `requires` File.hasContent "/srv/caddy/Caddyfile" $(sourceFile "files/caddy/Caddyfile")
    `requires` File.dirExists "/srv/caddy/data"
    `requires` File.dirExists "/srv/caddy"
    `requires` File.dirExists "/srv/tempus-backend"
    `requires` JayessSites.ownerGroupRecursive "/srv/postgresql" (User "999") (Group "999")
    `requires` File.hasPrivContent "/srv/postgresql/password" hostContext
    `requires` File.hasContent "/srv/postgresql/conf/pg_hba.conf" $(sourceFile "files/postgresql/pg_hba.conf")
    `requires` File.hasContent "/srv/postgresql/conf/postgresql.conf" $(sourceFile "files/postgresql/postgresql.conf")
    `requires` File.dirExists "/srv/postgresql/socket"
    `requires` File.dirExists "/srv/postgresql/conf"
    `requires` File.dirExists "/srv/postgresql/data"
    `requires` File.dirExists "/srv/postgresql"
    -- `onChange` Systemd.restarted "tempus-backend"


jayessKeys :: User -> Property UnixLike
jayessKeys user = propertyList "keys for jayess"
                  . toProps
                  . map (setupRevertableProperty . Ssh.authorizedKey user) $
                  [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsdT1r84ZlfcHyibUUZqrT1SkJ5JNBVd/Uwym4T2L2okUqqet+a+oxzIyclN2/9ay8hdk+Fmh4uro97F0Mu7OxX/K8qsa/C6B1wLWdYB4BrbHxhPs8edklPRVB0QaepYNtUZWvvb+TOekCRaG9KTYatAcd/0hfu6qiAk2RvIOJk0g8o2WWhIKoUUYUrAUuoBQhAhOtfcZGjriIiMWjkX0+NpGx6twpgQvQNYy7w39NrgP0IhnVd1gJw6mSIwCQM0p3ztvxI2x0Iwi8oFSq6UwZnHVIs7b7Gms+rv+Q7yXvCYaYvFBnM3a8VuVc7WFJ7CwEGxqtb+PBneeDzd/z2OeF cardno:000603012235"]


scotchKeys :: User -> Property UnixLike
scotchKeys user = propertyList "keys for scotch"
                . toProps
                . map (setupRevertableProperty . Ssh.authorizedKey user) $
                [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEA6S3FSzSQsTvaSNvZVo0F6Kw0fwEmknFZrQLTRDTgia39NHCuPcEDL6pSHo4M1s5EZnzrqUrhuxQ1or0ErsF2vZFoqSvjkeT3ORF5j1AxDQJKqtP3atxSDza12aXDUTCpqYTSnmj1vvW/ognmoMW7R+IJtXSPuA4oZ5UZ7ikZakD+rK/88kmoJP4obH1cW7jof3UYNliN3KFrAJpz3svJF/CqokZilw6VaKSdufoqICTVkjgbiEBuBeB1G90MEHek64j9nZdVr4XQN1pBh/wOavw70wI5i8x3WvHxGyaEaAPteuZ0yPuTQVuxrJbPEVnGgjFKYE8N2VZBMCMm027GkQ== rsa-key-20171113"
                ]


admins :: [User]
admins = map User [ "jayess"
                  , "scotch"
                  ]


adminKeys :: User -> Property UnixLike
adminKeys user = propertyList "admin keys" . toProps . map ($ user) $
                 [ jayessKeys
                 , scotchKeys
                 ]


dockerKey :: Apt.AptKey
dockerKey =
  Apt.AptKey "docker" $ unlines $(sourceFile "files/docker.asc")
