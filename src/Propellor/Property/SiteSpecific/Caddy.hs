{-# LANGUAGE FlexibleContexts, TypeFamilies #-}

module Propellor.Property.SiteSpecific.Caddy where

import Propellor.Base
import qualified Propellor.Property.File as File
import qualified Propellor.Property.Apt as Apt
import Utility.UserInfo
import qualified Propellor.Property.User as User
import Utility.Split
import Network.URI as URI

import Data.List
import System.Posix.Files

urlLastSegment :: String -> String
urlLastSegment urlString = Data.List.last $ URI.pathSegments $ fromJust maybeUri
  where
    maybeUri = URI.parseURI urlString


-- Will always download and install from a URL. Currently `caddy -version`
-- doesn't return a value so we can't know when upgrading shouldn't be done.
alwaysUpdate :: String -> Property DebianLike
alwaysUpdate url = installed' url


-- Install to `/usr/local/bin/caddy` if caddy isn't already installed.
installed :: Property DebianLike
installed = check (not <$> (doesFileExist "/usr/local/bin/caddy")) $ installed' url
    where
        url = "https://github.com/caddyserver/caddy/releases/download/v1.0.2/caddy_v1.0.2_linux_amd64.tar.gz"


-- Based on Caddy's `init/linux-systemd/README.md`
installed' :: String -> Property DebianLike
installed' url = combineProperties "Install Caddy" $ props
    & downloaded
    & untarred
    & cmdProperty "install" ["-t", "/usr/local/bin", "caddy"] `assume` MadeChange
    & cmdProperty "setcap" ["cap_net_bind_service=+ep", "/usr/local/bin/caddy"] `assume` MadeChange
    & User.systemAccountFor (User "www-data")
    & File.dirExists "/etc/caddy"
    & File.dirExists "/etc/ssl/caddy"
    & cmdProperty "chown" ["-R", "www-data:www-data", "/etc/ssl/caddy"] `assume` MadeChange
    & cmdProperty "chmod" ["0770", "/etc/ssl/caddy"] `assume` MadeChange
    where
      downloaded = cmdProperty "wget" [url] `assume` MadeChange `requires` Apt.installed ["wget"]
      untarred = cmdProperty "tar" ["-xzvf", filename] `assume` MadeChange
      filename = urlLastSegment url
